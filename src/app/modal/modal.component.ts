import { Component, OnInit } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { SharedService } from '../shared.service';
@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {
  winner:any
  constructor(private sharedService:SharedService,public ngxSmartModalService: NgxSmartModalService) { }

  ngOnInit() {
    this.sharedService.winner.subscribe(res=>this.openModal(res))
  } 

  openModal(winner){
    if(winner){
      this.winner = winner
      this.ngxSmartModalService.open('myModal')
    }
  }
  start(){
    this.sharedService.start();
    this.ngxSmartModalService.close('myModal')
  }

}
