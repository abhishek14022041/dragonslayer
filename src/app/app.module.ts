import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PlayerComponent } from './player/player.component';
import { DragonComponent } from './dragon/dragon.component';
import { ControlPanelComponent } from './control-panel/control-panel.component';
import { CommentryComponent } from './commentry/commentry.component';
import { SharedService } from './shared.service';

import { NgxSmartModalModule } from 'ngx-smart-modal';
import { ModalComponent } from './modal/modal.component';
@NgModule({
  declarations: [
    AppComponent,
    PlayerComponent,
    DragonComponent,
    ControlPanelComponent,
    CommentryComponent,
    ModalComponent
  ],
  imports: [
    BrowserModule,
    NgxSmartModalModule.forRoot()
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
