import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }
  playerPoint = new Subject();
  dragonPoint = new Subject();
  gameStart = new BehaviorSubject(false); //this will notify the components that game has started
  winner = new BehaviorSubject(null);     //this will notify the winner of the game
  replay = new BehaviorSubject(false);
  replayArray = [];
  playerAttack(PlayerPoint,DragonPoint){
    this.replayArray.push({'dragonPoint':-DragonPoint},{'playerPoint':-PlayerPoint})
    this.dragonPointChange(-DragonPoint);
    this.playerPointChange(-PlayerPoint);
  }
  heal(HealingPoint,PlayerPoint){
    this.replayArray.push({'playerPoint':HealingPoint},{'playerPoint':-PlayerPoint})
    this.playerPointChange(-PlayerPoint);
    this.playerPointChange(HealingPoint);
  }
  gameOver(winnerName){
    this.winner.next(winnerName)
    this.replay.next(true)
    this.gameStart.next(false);
  }
  giveUp(winnerName){
    this.winner.next(winnerName)
    this.gameStart.next(false);
  }
  dragonPointChange(points){
    this.dragonPoint.next(points);
  }
  playerPointChange(points){
    this.playerPoint.next(points);
  }
  start(){
    this.winner.next(null)
    this.gameStart.next(true)
  }
  replayGame(){
    if(this.replayArray.length){
      this.start();
      this.replay.next(true)
      setTimeout(()=>this.tiemOutFunc(this.replayArray),300)
    }
  }
  tiemOutFunc(arr){
    let ele = arr.shift(1)
    if(ele.playerPoint){
      this.playerPointChange(ele.playerPoint);
    }
    else if(ele.dragonPoint){
      this.dragonPointChange(ele.dragonPoint);
    }

    if(arr.length)
    setTimeout(()=>this.tiemOutFunc(arr),500)
    else this.replay.next(false)
    
  }
}
