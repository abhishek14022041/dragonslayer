import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-commentry',
  templateUrl: './commentry.component.html',
  styleUrls: ['./commentry.component.css']
})
export class CommentryComponent implements OnInit {
  Comments = [];
  gameOver ;
  constructor(private sharedService:SharedService) { }

  ngOnInit() {
    this.sharedService.dragonPoint.subscribe(res=>this.playerAttacks(res));
    this.sharedService.playerPoint.subscribe(res=>this.dragonAttacks(res));
    this.sharedService.winner.subscribe(res=>this.winnerDecided(res))
  }

  dragonAttacks(points){
    if(points<=0 && !this.gameOver){
    this.Comments.push(`Dragon Attacks Player For ${Math.abs(points)}`)
    }
    else if(!this.gameOver){
      this.Comments.push(`Player Healed by ${Math.abs(points)}`)
    }
  }

  playerAttacks(points){
    if(!this.gameOver)
    this.Comments.push(`Player Attacks Dragon For ${Math.abs(points)}`)
  }

  winnerDecided(winner){
    if(winner){
      this.Comments.push(`${winner} wins`);
      this.Comments = []
    }
    this.gameOver = winner
  }

}
