import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';
@Component({
  selector: 'app-dragon',
  templateUrl: './dragon.component.html',
  styleUrls: ['./dragon.component.css']
})
export class DragonComponent implements OnInit {
  life=100;
  gameOver;
  constructor(private sharedService:SharedService) { }

  ngOnInit() {

    this.sharedService.dragonPoint.subscribe(res=>this.lifeChange(res))
    this.sharedService.winner.subscribe(res=>this.gameOver = res)
    this.sharedService.gameStart.subscribe(res=>this.initalizeDragon(res));
  }

  initalizeDragon(res){
    if(res){
      this.life = 100;
    }
  }

  lifeChange(val){
    if(this.life>0&&!this.gameOver){
      this.life = this.life+val;
      if(this.life<=0){
        this.life = 0
        this.sharedService.gameOver("You"); 
      }
    }
  }
}
