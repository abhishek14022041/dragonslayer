import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {
  life=100;
  gameOver;
  constructor(private sharedService:SharedService) { }

  ngOnInit() {
    this.sharedService.playerPoint.subscribe(res=>this.lifeChange(res))
    this.sharedService.winner.subscribe(res=>this.gameOver = res)
    this.sharedService.gameStart.subscribe(res=>this.initalizePlayer(res));
  }
  initalizePlayer(res){
    if(res){
      this.life = 100;
    }
  }
  lifeChange(val){
    if(val<0 && !this.gameOver){
      if(this.life>0){
        this.life = this.life+val;
        if(this.life<0){
          this.life = 0
          this.sharedService.gameOver("Dragon");
        }
      }
    }
    else if(!this.gameOver){
      this.life = this.life+val;
      if(this.life>100){
        this.life=100
      }
    }
  }
}
