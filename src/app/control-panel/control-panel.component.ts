import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-control-panel',
  templateUrl: './control-panel.component.html',
  styleUrls: ['./control-panel.component.css']
})
export class ControlPanelComponent implements OnInit {
  start;
  controlPanel = false
  replay=false;
  constructor(private sharedService:SharedService) { }

  ngOnInit() {
    this.sharedService.gameStart.subscribe(res=>{
      this.start = res;
      if(res)
      this.controlPanel = true
    })
    this.sharedService.replay.subscribe(res=>{
      this.replay = res;
      if(res){
        this.controlPanel = false;
      }
    })
  }
  attack(){
    this.sharedService.playerAttack(this.getRandomArbitrary(0,10),this.getRandomArbitrary(0,10))
  }
  blast(){
    this.sharedService.playerAttack(this.getRandomArbitrary(0,30),this.getRandomArbitrary(0,30))
  }
  heal(){
    this.sharedService.heal(this.getRandomArbitrary(0,20),this.getRandomArbitrary(0,20))
  }
  getRandomArbitrary(min, max) {
    let random =  parseInt(Math.random() * (max - min) + min)
    return random
  }
  startGame(){
    this.sharedService.start();
  }
  giveUp(){
    this.controlPanel = false;
    this.sharedService.giveUp('Dragon')
  }
  showReplay(){
    this.sharedService.replayGame()
  }
  
}
