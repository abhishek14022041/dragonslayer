# UvsDragon

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) 
version 7.0.6.
This project is a single player game. You will have to defeat the dragon to win this game.
There are four controls
Attack :- Your player will attack the player for maximum of 10 points and 
is open for an attack from dragon
Blast  :- Your player will attack the player for maximum of 30 points and 
is open for an attack from dragon for the same points.
Heal   :- Your player will heal with maximum of 20 points but your player 
will be open for attack from dragon for the same points
Give Up :- You will loose the game.

You can view the highlights by pressing replay button when the match is over

# Architecture

The project is in angular to leverage angular functioalities the project 
is divided into multiple component and a shared service
Components

1.Dragon Component:- Have the code related to the dragon functioalities 
and the view for dragon the dragon shape and size can be change from here

2.Player Component:- Have the code related to the player functioalities 
like life lost,heal and the view for player the player shape and size  can be change from here

3.ControlPanel Component: Have the code for attack,heal,blast and giveup 

4.Commentry Component: This is the running commentry box where player can view all
the attacks done in present game

Services
1.Shared Service:- Most of the functonalites are shared between the above components 
so to pass data from one component to other we have used services which makes the data consistent 
in all the components and we can control the data from one place.

#Steps taken to build the project
1.We designed the different components 
2.Now we add functionalites of every button in control panel
    a.Attack:- This will fire the attack function in control panel component which will fire
                a playerAttack function in shared service which takes two argument of type number.
                These will be random number which defines the point lost by player and dragon

                The service function will take these pints and notify the player and dragon compnent that to 
                reduce the life of the two by the given points
    b.Blast:- This will also do the same thing as attack but the points difference will be larger
    c.Heal :- In attack and blast we send a negative value to the component so they can be reduced from the
              life but in this method we will send a positive value so as to increase the life of player

    These points are pushed in an array which is then used to show the commentry and replay of the game           

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


